import React, { Dispatch, SetStateAction, useState } from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Home from '../homestuff/Home';
import LoginForm from '../loginstuff/LoginForm';

export default function Navigation(props: {username: string, setUsername: Dispatch<SetStateAction<string>>;}) {

    

    return (
        <Router>
            <Switch>
                <Route path="/Home"><Home username={props.username}></Home></Route>
                <Route path="/"><LoginForm username={props.username} setUsername={props.setUsername}></LoginForm></Route>
            </Switch>
        </Router>

    );
}

