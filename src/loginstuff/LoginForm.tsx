import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Container from 'react-bootstrap/Container';
import 'bootstrap/dist/css/bootstrap.css';
import { Dispatch, SetStateAction, useState } from "react";
import { useHistory } from "react-router-dom";

function LoginForm(props: {username: string, setUsername: Dispatch<SetStateAction<string>>;}) {
    const [password, setPassword] = useState("");

    const history = useHistory();

    const validateForm = () => {
        return props.username.length > 0 && password.length > 0;
    };

    const tryLogin = () => {
        history.push("/Home");
    };

    return (<div>
        <Container className="form-container">
            <Form onSubmit={() => tryLogin()}>
                <Form.Group controlId="username">
                    <Form.Label>Username</Form.Label>
                    <Form.Control
                        autoFocus
                        type="text"
                        value={props.username}
                        onChange={(e) => props.setUsername(e.target.value)}
                    />
                </Form.Group>
                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </Form.Group>
                <br />
                <Button size="lg" type="submit" variant="outline-primary" disabled={!validateForm()}>
                    Login
                </Button>

            </Form>
            <br />
            <Button size="sm" variant="outline-dark">
                Sign Up
            </Button>
        </Container>
    </div>);
}

export default LoginForm;