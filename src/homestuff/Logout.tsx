import { useHistory } from "react-router-dom";
import Button from "react-bootstrap/Button";
import 'bootstrap/dist/css/bootstrap.css';

export default function Logout() {
    const history = useHistory();

    function logMeOut() {
        history.push("/");
    }

    return (
        <Button onClick={() => logMeOut()} variant="outline-dark">
            Logout
        </Button>
    );
}