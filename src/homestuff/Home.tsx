import Logout from './Logout';

export default function Home(props: {username: string}) {
    return (<div>
        <h1>
            Welcome {props.username}.
        </h1>
        <Logout/>
    </div>);
}