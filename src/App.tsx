import './App.css';
import Navigation from './navstuff/Navigation';
import { useState } from "react";

function App() {

  const [username, setUsername] = useState("");

  return (
    <div className="App">
        <Navigation username={username} setUsername={setUsername}/>
    </div>
  );
}

export default App;
